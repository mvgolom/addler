from flask import Flask, flash, session, request, render_template, redirect, url_for, Response,jsonify
from pymongo import MongoClient
import json

app = Flask(__name__)
app.secret_key = 'M@rekdsd*&6465445646asd!#$%'

#shopping
#za3LLGsjg77Uacm
client = MongoClient("mongodb+srv://shopping:za3LLGsjg77Uacm@mvgolom-0ixzy.mongodb.net/buyList?retryWrites=true&w=majority")
db = client.buyList
#mongodb+srv://shopping:za3LLGsjg77Uacm@mvgolom-0ixzy.mongodb.net/buyList?retryWrites=true&w=majority

@app.route("/")
def index():
    checked = list(db.lists.find({},{"_id":0}))
    content_page = {}
    content_page["checked"] = checked
    return render_template("index.html",content_page = content_page)

@app.route("/create")
def createlist():
    content_page = []
    return render_template("viewlist.html",content_page = content_page)


@app.route('/save/list', methods=['GET', 'POST'])
def add_list():
    if request.method == "POST":
        content = request.json[0]
        namelist = content.get("NameList")
        itemList = content.get("ListContent")
        for item in itemList:
            if(item.get("name") != ""):
                name = item.get("name")
                metric = item.get("metric")
                qtde = item.get("qtde")
                price = item.get("price")
                db[namelist].insert({"name":name,"metric": metric,"qtde": qtde,"price":price})
        return "ok",200

@app.route('/create/list', methods=['GET', 'POST'])
def create():
    if request.method == "POST":
        name = request.form.get('name')
        date = request.form.get('creationdate')
        db.lists.insert({"name":name,"date":date})
        content_page = {
            "name":name
        }
        return render_template("createList.html",content_page = content_page)

@app.route('/view/list', methods=['GET', 'POST'])
def view():
    if request.method == "POST":
        name = request.form.get('name')
        checked = list(db[name].find({},{"_id":0}))
        content_page = {
            "name":name,
            "checked":checked
        }
        return render_template("viewlist.html",content_page = content_page)

@app.route('/api/view/list', methods=['GET', 'POST'])
def apiview():
    if request.method == "POST":
        name = request.form.get('name')
        checked = list(db[name].find({},{"_id":0}))
        content = {
            "name":name,
            "content":checked
        }
        return json_response(jsonify(content))

@app.route("/api/lists", methods=['GET', 'POST'])
def listLists():
    if request.method == "POST":
        checked = list(db.lists.find({},{"_id":0}))
        return json_response(jsonify(checked))


def json_response(payload, status=200):
    return (payload, status, {'content-type': 'application/json'})


if __name__ == "__main__":
    app.run(debug=True)